<?php

namespace Drupal\webform_jira\Form;

use Drupal\Component\Utility\Unicode;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Url;
use Drupal\Core\Entity\BundleEntityFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\webform\Form\WebformDialogFormTrait;
use Drupal\webform\Utility\WebformYaml;
use Drupal\webform\WebformTokenManagerInterface;

/**
 * Class WebformJiraFieldForm
 *
 * @package Drupal\webform_jira\Form
 */
class WebformJiraFieldForm extends BundleEntityFormBase {

  use WebformDialogFormTrait;

  /**
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * @var \Drupal\webform\WebformTokenManagerInterface
   */
  protected $webformTokenManager;

  /**
   * WebformJiraFieldForm constructor.
   *
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   * @param \Drupal\webform\WebformTokenManagerInterface $webform_token_manager
   */
  public function __construct(MessengerInterface $messenger, WebformTokenManagerInterface $webform_token_manager) {
    $this->messenger = $messenger;
    $this->webformTokenManager = $webform_token_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('messenger'),
      $container->get('webform.token_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#description' => $this->t('Administrative label.'),
      '#required' => TRUE,
    ];
    $form['key'] = [
      '#type' => 'machine_name',
      '#title' => $this->t('Key'),
      '#machine_name' => [
        'label' => '<br/>' . $this->t('Key'),
        'exists' => [$this, 'exists'],
        'source' => ['label'],
      ],
      '#required' => TRUE,
    ];

    $form['mapping_type'] = [
      '#type' => 'radios',
      '#title' => $this->t('Field mapping type'),
      '#description' => $this->t(
        'Primary type fields are the basic fields to create an issue. Likely you need the fields project, summary and issue type. Custom fields have been created by the Jira administrator.'
      ),
      '#options' => [
        'primary' => $this->t('Primary'),
        'custom' => $this->t('Custom'),
      ],
      '#required' => TRUE,
    ];

    $form['custom'] = [
      '#title' => $this->t('Custom Field information'),
      '#type' => 'fieldset',
      '#attributes' => [
        'class' => ['field-type-custom'],
      ],
      '#states' => [
        'visible' => [
          ':input[name="mapping_type"]' => ['value' => 'custom'],
        ],
        'required' => [
          ':input[name="mapping_type"]' => ['value' => 'custom'],
        ],
      ],
    ];

    $form['custom']['custom_field_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Jira field id'),
      '#description' => $this->t('The corresponding Jira field id (probably it looks something like customfield_123456)'),
      '#states' => [
        'visible' => [
          ':input[name="mapping_type"]' => ['value' => 'custom'],
        ],
        'required' => [
          ':input[name="mapping_type"]' => ['value' => 'custom'],
        ],
      ],
    ];

    $form['custom']['custom_field_type'] = [
      '#type' => 'select',
      '#options' => $this->getCustomFieldOptions(),
      '#title' => $this->t('Jira field type'),
      '#description' => $this->t('The field type in Jira.'),
      '#states' => [
        'visible' => [
          ':input[name="mapping_type"]' => ['value' => 'custom'],
        ],
        'required' => [
          ':input[name="mapping_type"]' => ['value' => 'custom'],
        ],
      ],
    ];

    $form['primary'] = [
      '#title' => $this->t('Primary Field information'),
      '#type' => 'fieldset',
      '#attributes' => [
        'class' => ['field-type-primary'],
      ],
      '#states' => [
        'visible' => [
          ':input[name="mapping_type"]' => ['value' => 'primary'],
        ],
        'required' => [
          ':input[name="mapping_type"]' => ['value' => 'primary'],
        ],
      ],
    ];
    $form['primary']['primary_field_id'] = [
      '#type' => 'select',
      '#options' => $this->getPrimaryFieldOptions(),
      '#title' => $this->t('Jira field id'),
      '#description' => $this->t('The corresponding primary Jira field id. Note: "Reporter" requires the "Manage Reporter" permission to be enabled.'),
      '#states' => [
        'visible' => [
          ':input[name="mapping_type"]' => ['value' => 'primary'],
        ],
        'required' => [
          ':input[name="mapping_type"]' => ['value' => 'primary'],
        ],
      ],
    ];

    $form['value'] = [
      '#title' => $this->t('Value information'),
      '#type' => 'fieldset',
      '#attributes' => [
        'class' => ['field-value-information'],
      ],
    ];
    $form['value']['value_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Field value type'),
      '#options' => [
        'webform_element' => $this->t('Webform element'),
        'text' => $this->t('Text'),
      ],
      '#required' => TRUE,
    ];

    $form['value']['text'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Text value'),
      '#states' => [
        'visible' => [
          ':input[name="value_type"]' => ['value' => 'text'],
        ],
        'required' => [
          ':input[name="value_type"]' => ['value' => 'text'],
        ],
      ],
    ];

    if($token_tree = $this->webformTokenManager->buildTreeElement()) {
      $form['value']['token_tree_link'] = $token_tree;
      $form['value']['token_tree_link']['#states'] = [
        'visible' => [
          ':input[name="value_type"]' => ['value' => 'text'],
        ],
        'required' => [
          ':input[name="value_type"]' => ['value' => 'text'],
        ],
      ];
    }

    $form['value']['webform_element'] = [
      '#type' => 'select',
      '#title' => $this->t('Webform element'),
      '#options' => $this->getWebformElementOptions(),
      '#description' => $this->t('Select the webform element. These elements can be found at the elements section.'),
      '#states' => [
        'visible' => [
          ':input[name="value_type"]' => ['value' => 'webform_element'],
        ],
        'required' => [
          ':input[name="value_type"]' => ['value' => 'webform_element'],
        ],
      ],
    ];

    $form = parent::buildForm($form, $form_state);
    if (!empty($form['actions']['delete'])) {
      unset($form['actions']['delete']);
    }
    return $this->buildDialogForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $value_type =  $form_state->getValue('value_type');
    if (empty($form_state->getValue($value_type))) {
      $form_state->setErrorByName($value_type, $this->t(
        'Field @value_type is required',
        ['@value_type' => $form['value'][$value_type]['#title']])
      );
    }

    $mapping_type = $form_state->getValue('mapping_type');
    if ($mapping_type === 'custom') {
      $custom_field_id = $form_state->getValue('custom_field_id');
      if (array_key_exists($custom_field_id, $this->getPrimaryFieldOptions())) {
        $form_state->setErrorByName('custom_field_id', $this->t(
          "Field id <strong>@custom_field_id</strong> is a reserved primary field id (reserved primary field id's are @primary_field_ids)",
          [
            '@custom_field_id' => $form_state->getValue('custom_field_id'),
            '@primary_field_ids' => implode(', ', array_keys($this->getPrimaryFieldOptions()))
          ]
        ));
        return;
      }
      if ($this->checkFieldExistenceByFieldId($custom_field_id)) {
        $form_state->setErrorByName('custom_field_id', $this->t(
          "Field id <strong>@custom_field_id</strong> already exist please choose a different field id",
          ['@custom_field_id' => $form_state->getValue('custom_field_id')]
        ));
      }
    }

    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $webform = $this->getEntity();
    $form_state->setRedirectUrl(Url::fromRoute(
      'webform_jira.configuration',
      ['webform' => $webform->id()]
    ));
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {

    /** @var \Drupal\webform\WebformInterface $webform */
    $webform = $this->getEntity();

    $fields = $this->processField($form_state) + $this->getFields();

    $webform->setThirdPartySetting(
      'webform_jira',
      'fields',
      WebformYaml::encode($fields)
    );
    $this->setEntity($webform);
    $this->messenger->addStatus($this->t(
      'Jira mapping saved for @label.',
      ['@label' => $webform->label()]
    ));

    parent::save($form, $form_state);
  }

  /**
   * @return array
   */
  protected function getPrimaryFieldOptions() {
    return [
      'project' => $this->t('Project'),
      'summary' => $this->t('Summary'),
      'assignee_name' => $this->t('Assignee name'),
      'priority_name' => $this->t('Priority name'),
      'issue_type' => $this->t('Issue Type'),
      'description' => $this->t('Description'),
      'version' => $this->t('Version'),
      'components' => $this->t('Components'),
      'security_id' => $this->t('Security id'),
      'due_date' => $this->t('Due date'),
      'reporter' => $this->t('Reporter')
    ];
  }

  /**
   * @return array
   */
  protected function getCustomFieldOptions() {
    return [
      'string' => $this->t('String'),
      'option' => $this->t('Option List (single choice)'),
      'option_multiple' => $this->t('Option List (multiple)'),
      'number' => $this->t('Number'),
    ];
  }

  /**
   * @return array
   */
  protected function getWebformElementOptions() {
    $element_options = [];
    /** @var \Drupal\webform\WebformInterface $webform */
    $webform = $this->getEntity();
    $decoded_elements = $webform->getElementsDecodedAndFlattened();
    if ($decoded_elements) {
      foreach ($decoded_elements as $key => $element) {
        $element_options[$key] = isset($element['#title']) ? Unicode::truncate($element['#title'], 30, TRUE, TRUE) . ' (key: ' . $key . ')' : $element['#type'];
      }
    }
    return $element_options;
  }

  /**
   * @return array
   */
  protected function getFields() {
    /** @var \Drupal\webform\WebformInterface $webform */
    $webform = $this->getEntity();
    return (array) WebformYaml::decode($webform->getThirdPartySetting(
      'webform_jira',
      'fields'
    ));
  }

  /**
   * @param string $key
   *
   * @return array|null
   */
  protected function getFieldByKey(string $key) {
    $field = NULL;
    $fields = $this->getFields();
    if (isset($fields[$key])) {
      $field = $fields[$key];
    }
    return $field;
  }

  /**
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return array
   */
  protected function processField(FormStateInterface $form_state) {
    $new_field = [];
    $value_type = $form_state->getValue('value_type');
    $new_field[$form_state->getValue('key')] = [
      'label' => $form_state->getValue('label'),
      'value_type' => $value_type,
      'value' => $form_state->getValue($value_type),
    ];
    $mapping_type = $form_state->getValue('mapping_type');
    $new_field[$form_state->getValue('key')]['mapping_type'] = $mapping_type;
    if ($mapping_type === 'custom') {
      $new_field[$form_state->getValue('key')]['field_type'] = $form_state->getValue('custom_field_type');
      $new_field[$form_state->getValue('key')]['field_id'] = $form_state->getValue('custom_field_id');
    }
    if ($mapping_type === 'primary') {
      $new_field[$form_state->getValue('key')]['field_type'] = 'string';
      $new_field[$form_state->getValue('key')]['field_id'] = $form_state->getValue('primary_field_id');
    }
    return $new_field;
  }

  /**
   * @param $field_id
   *
   * @return array
   */
  protected function checkFieldExistenceByFieldId($field_id) {
    $fields = $this->getFields();
    $keys = array_keys(
      array_filter(array_combine(
          array_keys($fields),
          array_column($fields, 'field_id'))
      ),
      $field_id
    );

    return array_intersect_key($fields, array_flip($keys));
  }

  /**
   * @param $key
   *
   * @return bool
   */
  public function exists($key) {
    return $this->getFieldByKey($key) ? TRUE : FALSE;
  }

}
