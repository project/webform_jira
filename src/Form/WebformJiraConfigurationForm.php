<?php

namespace Drupal\webform_jira\Form;

use Drupal\Component\Utility\Unicode;
use Drupal\Core\Entity\BundleEntityFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\webform\Form\WebformEntityAjaxFormTrait;
use Drupal\webform\Utility\WebformDialogHelper;
use Drupal\webform\Utility\WebformYaml;

/**
 * Class WebformJira
 *
 * @package Drupal\webform_jira\Form
 */
class WebformJiraConfigurationForm extends BundleEntityFormBase {
  use WebformEntityAjaxFormTrait;

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\webform\WebformInterface $webform */
    $webform = $this->getEntity();

    $form['jira_configuration'] = [
      '#type' => 'fieldset',
      '#attributes' => [
        'class' => ['jira-mapping'],
      ],
    ];

    $form['jira_configuration']['jira_status'] = [
      '#type' => 'checkbox',
      '#title' => t('Activate Jira service'),
      '#description' => $this->t('Submission data will be send to Jira if activated.'),
      '#default_value' => $webform->getThirdPartySetting('webform_jira', 'status'),
    ];

    $form['jira_configuration']['endpoint'] = [
      '#type' => 'jira_endpoint_select',
      '#title' => t('Jira service'),
      '#description' => $this->t('The endpoint to connect to JIRA'),
      '#default_value' => $webform->getThirdPartySetting('webform_jira', 'endpoint'),
    ];

    $form['jira_configuration']['save_issue_key'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Save issue key to Webform submission element'),
      '#default_value' => $webform->getThirdPartySetting('webform_jira', 'save_issue_key'),
    ];

    $form['jira_configuration']['issue_key_webform_element'] = [
      '#type' => 'select',
      '#title' => $this->t('Webform element'),
      '#options' => $this->getWebformElementOptions(),
      '#default_value' => $webform->getThirdPartySetting('webform_jira', 'issue_key_webform_element'),
      '#empty_option' => $this->t('- None -'),
      '#description' => $this->t('Select the Webform element for saving the Jira Issue key.'),
      '#states' => [
        'visible' => [
          ':input[name="save_issue_key"]' => ['checked' => TRUE],
        ],
        'required' => [
          ':input[name="save_issue_key"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['jira_configuration']['add_attachments'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Add attachments to issue'),
      '#default_value' => $webform->getThirdPartySetting('webform_jira', 'add_attachments'),
    ];

    $form['jira_configuration']['attachment_webform_elements'] = [
      '#type' => 'select',
      '#multiple' => TRUE,
      '#title' => $this->t('Webform element'),
      '#options' => $this->getWebformElementsManagedFiles(),
      '#default_value' => $webform->getThirdPartySetting('webform_jira', 'attachment_webform_elements'),
      '#empty_option' => $this->t('- None -'),
      '#description' => $this->t('Select the Webform managed file elements to attach to a Jira issue.'),
      '#states' => [
        'visible' => [
          ':input[name="add_attachments"]' => ['checked' => TRUE],
        ],
        'required' => [
          ':input[name="add_attachments"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['webform_jira_fields'] = [
      '#type' => 'table',
      '#header' => $this->getTableHeader(),
      '#empty' => $this->t('Please add Jira mapping fields.'),
      '#attributes' => [
        'class' => ['webform-jira-table'],
      ],
    ] + $this->getTableRows();

    WebformDialogHelper::attachLibraries($form);

    $form['#attached']['library'][] = 'webform_ui/webform_ui';
    $form = parent::buildForm($form, $form_state);

    return $this->buildAjaxForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (
      $form_state->getValue('save_issue_key') &&
      empty($form_state->getValue('issue_key_webform_element'))
    ) {
      $form_state->setErrorByName(
        'issue_key_webform_element',
        $this->t('Field Webform element can not be empty if Save issue key to Webform submission element field is selected.')
      );
    }
    if (
      $form_state->getValue('add_attachments') &&
      empty($form_state->getValue('attachment_webform_elements'))
    ) {
      $form_state->setErrorByName(
        'attachment_webform_element',
        $this->t('Field Webform managed file elements can not be empty if field Add attachments to issue is selected.')
      );
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\webform\WebformInterface $webform */
    $webform = $this->getEntity();

    $webform->setThirdPartySetting(
      'webform_jira',
      'status',
      (bool) $form_state->getValue('jira_status')
    );

    $webform->setThirdPartySetting(
      'webform_jira',
      'endpoint',
      $form_state->getValue('endpoint')
    );

    $webform->setThirdPartySetting(
      'webform_jira',
      'save_issue_key',
      (bool) $form_state->getValue('save_issue_key')
    );

    $webform->setThirdPartySetting(
      'webform_jira',
      'issue_key_webform_element',
      $form_state->getValue('issue_key_webform_element')
    );

    $webform->setThirdPartySetting(
      'webform_jira',
      'add_attachments',
      (bool) $form_state->getValue('add_attachments')
    );

    $webform->setThirdPartySetting(
      'webform_jira',
      'attachment_webform_elements',
      $form_state->getValue('attachment_webform_elements')
    );

    $this->setEntity($webform);

    $this->messenger()->addStatus($this->t('Jira fields for Webform @label saved.', [
      '@label' => $webform->label(),
    ]));

    return parent::save($form, $form_state);
  }

  /**
   * Gets the Jira table header.
   *
   * @return array
   *   The header elements.
   */
  protected function getTableHeader() {
    $header = [];
    $header['key'] = $this->t('Key');
    $header['label'] = $this->t('Label');
    $header['jira_field_id'] = $this->t('Jira Field id');
    $header['jira_field_type'] = $this->t('Jira Field Type');
    $header['value_type'] = $this->t('Value type');
    $header['Value'] = $this->t('Value');
    $header['mapping_type'] = $this->t('Mapping type');
    $header['operations'] = [
      'data' => $this->t('Operations'),
      'class' => ['webform-jira-operations'],
    ];
    return $header;
  }

  /**
   * Gets the elements table header.
   *
   * @return array
   *   The header elements.
   */
  protected function getTableRows() {
    /** @var \Drupal\webform\WebformInterface $webform */
    $webform = $this->getEntity();
    $fields = (array) WebformYaml::decode($webform->getThirdPartySetting(
      'webform_jira',
      'fields'
    ));

    $rows = [];
    foreach ($fields as $key => $field) {
      $rows[$key] = [
        'key' => ['#markup' => $key],
        'label' => ['#markup' => $field['label']],
        'field_id' => ['#markup' => $field['field_id']],
        'field_type' => ['#markup' => $field['field_type']],
        'value_type' => ['#markup' => $field['value_type']],
        'value' => ['#markup' => $field['value']],
        'mapping_type' => ['#markup' => $field['mapping_type']],
        'operations' => [
          '#type' => 'operations',
          '#prefix' => '<div class="webform-dropbutton">',
          '#suffix' => '</div>',
          '#links' => [
            'edit' => [
              'title' => $this->t('Edit'),
              'url' => new Url('webform_jira.edit_field', [
                  'webform' => $webform->id(),
                  'key' => $key,
              ]),
              'attributes' => WebformDialogHelper::getModalDialogAttributes(),
            ]
          ]
        ],
      ];
    }
    return $rows;
  }

  /**
   * @return array
   */
  protected function getWebformElementOptions() {
    $element_options = [];
    /** @var \Drupal\webform\WebformInterface $webform */
    $webform = $this->getEntity();
    $decoded_elements = $webform->getElementsDecodedAndFlattened();
    if ($decoded_elements) {
      foreach ($decoded_elements as $key => $element) {
        $element_options[$key] = isset($element['#title']) ? Unicode::truncate($element['#title'], 30, TRUE, TRUE) . ' (key: ' . $key . ')' : $element['#type'];
      }
    }
    return $element_options;
  }

  /**
   * @return array
   */
  protected function getWebformElementsManagedFiles() {
    /** @var \Drupal\webform\WebformInterface $webform */
    $webform = $this->getEntity();
    $managed_files_elements = $webform->getElementsManagedFiles();
    $decoded_managed_files_elements = [];
    if ($managed_files_elements) {
      $decoded_elements = $webform->getElementsDecodedAndFlattened();
      $decoded_managed_files_elements = array_intersect_key($decoded_elements, $managed_files_elements);
    }

    $element_options = [];
    if ($decoded_managed_files_elements) {
      foreach ($decoded_managed_files_elements as $key => $element) {
        $element_options[$key] = isset($element['#title']) ? $element['#title'] . ' (' . $element['#type'] . ')' : $element['#type'];
      }
    }
    return $element_options;
  }

  /**
   * {@inheritdoc}
   */
  protected function actionsElement(array $form, FormStateInterface $form_state) {
    $form = parent::actionsElement($form, $form_state);
    $form['submit']['#value'] = $this->t('Save Jira settings');
    unset($form['delete']);
    unset($form['reset']);
    return $form;
  }

}
