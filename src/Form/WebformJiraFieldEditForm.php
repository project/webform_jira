<?php

namespace Drupal\webform_jira\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class WebformJiraFieldEditForm
 *
 * @package Drupal\webform_jira\Form
 */
class WebformJiraFieldEditForm extends WebformJiraFieldForm {

  /**
   * The field.
   *
   * @var array
   */
  protected $field = [];

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $key = NULL) {
    $field = $this->getFieldByKey($key);
    if (!$field) {
      throw new NotFoundHttpException();
    }

    $this->field = $field;

    $form = parent::buildForm($form, $form_state);
    $form['label']['#default_value'] = $field['label'];
    $form['key']['#default_value'] = $key;
    $form['key']['#disabled'] = TRUE;

    $mapping_type = $field['mapping_type'];
    $form['mapping_type']['#default_value'] = $mapping_type;
    $form[$mapping_type][$mapping_type . '_field_type']['#default_value'] = $field['field_type'];
    $form[$mapping_type][$mapping_type . '_field_id']['#default_value'] = $field['field_id'];

    $value_type =  $field['value_type'];
    $form['value']['value_type']['#default_value'] = $value_type;
    $form['value'][$value_type]['#default_value'] = $field['value'];

    $url = new Url('webform_jira.field.delete', ['webform' => $this->getEntity()->id(), 'key' => $key]);
    $this->buildDialogDeleteAction($form, $form_state, $url);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $value_type =  $form_state->getValue('value_type');
    if (empty($form_state->getValue($value_type))) {
      $form_state->setErrorByName($value_type, $this->t(
        'Field @value_type is required',
        ['@value_type' => $form[$value_type]['#title']])
      );
    }

    $field_id = $form_state->getValue($form_state->getValue('mapping_type') .'_field_id');
    if ($this->field['field_id'] !== $field_id) {
      return parent::validateForm($form, $form_state);
    }
  }

}
