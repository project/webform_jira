<?php

namespace Drupal\webform_jira\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\webform\Form\WebformDeleteFormBase;
use Drupal\webform\Utility\WebformYaml;
use Drupal\webform\WebformInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class WebformJiraFieldDeleteForm
 *
 * @package Drupal\webform_jira\Form
 */
class WebformJiraFieldDeleteForm extends WebformDeleteFormBase {

  /**
   * @var \Drupal\webform\WebformInterface
   */
  protected $webform;

  /**
   * The Jira field.
   *
   * @var array
   */
  protected $field = [];

  /**
   * The Jira field id.
   *
   * @var string
   */
  protected $key = NULL;

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    $t_args = [
      '@title' => $this->field['label'],
    ];
    return $this->t("Delete the '@title' field?", $t_args);
  }

  /**
   * {@inheritdoc}
   */
  public function getWarning() {
    $t_args = ['%title' => $this->field['label']];
    return [
      '#type' => 'webform_message',
      '#message_type' => 'warning',
      '#message_message' => $this->t('Are you sure you want to delete the %title Jira field?', $t_args) . '<br/>' .
        '<strong>' . $this->t('This action cannot be undone.') . '</strong>',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return Url::fromRoute(
      'webform_jira.configuration',
      ['webform' => $this->webform->id()]
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'webform_jira_field_delete_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, WebformInterface $webform = NULL, $key = NULL) {
    $this->webform = $webform;
    $this->key = $key;
    $this->field = $this->getFieldByKey($key);

    if (!$this->field) {
      throw new NotFoundHttpException();
    }

    $form = parent::buildForm($form, $form_state);
    $form = $this->buildDialogConfirmForm($form, $form_state);
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->deleteField();
    $this->webform->save();

    $this->messenger()->addStatus($this->t('The webform Jira field %title has been deleted.', ['%title' => $this->field['label']]));
    $form_state->setRedirectUrl(Url::fromRoute(
      'webform_jira.configuration',
      ['webform' => $this->webform->id()]
    ));
  }

  /**
   * @param string $key
   *
   * @return array|null
   */
  protected function getFieldByKey(string $key) {
    $field = NULL;
    $fields = $this->getFields();
    if (isset($fields[$key])) {
      $field = $fields[$key];
    }
    return $field;
  }

  /**
   * @return array
   */
  protected function getFields() {
    /** @var \Drupal\webform\WebformInterface $webform */
    $webform = $this->webform;
    return (array) WebformYaml::decode($webform->getThirdPartySetting(
      'webform_jira',
      'fields'
    ));
  }

  /**
   *
   */
  protected function deleteField() {
    $fields = $this->getFields();
    unset($fields[$this->key]);

    $this->webform->setThirdPartySetting(
      'webform_jira',
      'fields',
      $fields ? WebformYaml::encode($fields) : ''
    );
  }

}
