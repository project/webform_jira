<?php

namespace Drupal\webform_jira;

use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\jira_rest\JiraRestWrapperService;
use Drupal\webform\Plugin\WebformElementManagerInterface;
use Drupal\webform\Utility\WebformYaml;
use Drupal\webform\WebformSubmissionInterface;
use Drupal\webform\WebformTokenManagerInterface;
use JiraRestApi\Issue\Issue;
use JiraRestApi\Issue\IssueField;
use JiraRestApi\JiraException;

/**
 * Class WebformJiraService
 *
 * @package Drupal\webform_jira
 */
class WebformJiraService {
  use StringTranslationTrait;

  /**
   * @var \Drupal\jira_rest\JiraRestWrapperService
   */
  protected $jiraService;

  /**
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $loggerWebformJira;

  /**
   * @var \Drupal\webform\WebformTokenManagerInterface
   */
  protected $webformTokenManager;

  /**
   * @var \Drupal\webform\Plugin\WebformElementManagerInterface
   */
  protected $webformElementManager;

  /**
   * WebformJiraService constructor.
   *
   * @param \Drupal\jira_rest\JiraRestWrapperService $jira_service
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   * @param \Drupal\webform\WebformTokenManagerInterface $webform_token_manager
   */
  public function __construct(JiraRestWrapperService $jira_service, LoggerChannelFactoryInterface $logger_factory, WebformTokenManagerInterface $webform_token_manager, WebformElementManagerInterface $webform_element_manager) {
    $this->loggerWebformJira = $logger_factory->get('webform_jira');
    $this->webformTokenManager = $webform_token_manager;
    $this->webformElementManager = $webform_element_manager;
    $this->jiraService = $jira_service;
  }

  /**
   * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
   *
   * @return \JiraRestApi\Issue\Issue|null
   */
  public function createJiraIssueBySubmission(WebformSubmissionInterface $webform_submission) {
    $webform = $webform_submission->getWebform();
    $fields = (array) WebformYaml::decode($webform->getThirdPartySetting(
      'webform_jira',
      'fields'
    ));

    $issue = NULL;
    if ($fields) {
      try {
        $issue_field = new IssueField();
        $issue_field = $this->setPrimaryFieldsToIssueField(
          $issue_field,
          $fields,
          $webform_submission
        );
        $issue_field = $this->setCustomFieldsToIssueField(
          $issue_field,
          $fields,
          $webform_submission
        );
        $issue = $this->jiraService->getIssueService($webform->getThirdPartySetting('webform_jira', 'endpoint'))->create($issue_field);

        if ($issue && $webform->getThirdPartySetting('webform_jira', 'add_attachments')) {
          $this->addAttachments($issue, $webform_submission);
        }

      } catch (\Exception $e) {
        $this->loggerWebformJira->error($e->getMessage());
      }
    }
    return $issue;
  }

  /**
   * @param \JiraRestApi\Issue\IssueField $issue_field
   * @param array $fields
   * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
   *
   * @return \JiraRestApi\Issue\IssueField
   * @throws \Exception
   */
  protected function setPrimaryFieldsToIssueField(IssueField $issue_field, array $fields, WebformSubmissionInterface $webform_submission) {
    $primary_fields = $this->getFieldsByMappingTypeId($fields, 'primary');
    foreach ($primary_fields as $primary_field) {
      $method_name = $this->getMethodNameByPrimaryFieldId($primary_field['field_id']);
      $value = $this->processFieldAndSubmissionDataToValue($primary_field, $webform_submission);
      if ($method_name and $value) {
        $issue_field->{$method_name}($value);
      }
    }

    return $issue_field;
  }

  /**
   * @param \JiraRestApi\Issue\IssueField $issue_field
   * @param array $jira_fields
   * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
   *
   * @return \JiraRestApi\Issue\IssueField
   * @throws \Exception
   */
  protected function setCustomFieldsToIssueField(IssueField $issue_field, array $jira_fields, WebformSubmissionInterface $webform_submission) {
    $custom_fields = $this->getFieldsByMappingTypeId($jira_fields, 'custom');
    foreach ($custom_fields as $custom_field) {
      if ($value = $this->processFieldAndSubmissionDataToValue($custom_field, $webform_submission)) {
        $issue_field->addCustomField($custom_field['field_id'], $value);
      }
    }

    return $issue_field;
  }

  /**
   * @param $fields
   * @param $mapping_type
   *
   * @return array
   */
  protected function getFieldsByMappingTypeId($fields, $mapping_type) {
    $keys = array_keys(
      array_filter(array_combine(
        array_keys($fields),
        array_column($fields, 'mapping_type'))
      ),
      $mapping_type
    );
    return array_intersect_key($fields, array_flip($keys));
  }

  /**
   * @param $field_id
   *
   * @return string|null
   */
  protected function getMethodNameByPrimaryFieldId($field_id) {
    $primary_fields = [
      'project' => 'setProjectKey',
      'summary' => 'setSummary',
      'assignee_name' => 'setAssigneeName',
      'priority_name' => 'setPriorityName',
      'issue_type' => 'setIssueType',
      'description' => 'setDescription',
      'version' => 'addVersion',
      'components' => 'addComponents',
      'security_id' => 'setSecurityId',
      'due_date' => 'setDueDate',
      'reporter' => 'setReporterName',
    ];
    return isset($primary_fields[$field_id]) ? $primary_fields[$field_id] : NULL;
  }

  /**
   * @param $field
   * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
   *
   * @return array|null|string
   * @throws \Exception
   */
  protected function processFieldAndSubmissionDataToValue($field, WebformSubmissionInterface $webform_submission) {
    if (empty($field['field_type'])) {
      throw new \Exception($this->t('field_type not specified in field'));
    }

    if ($value = $this->getValueByValueType($field, $webform_submission)) {
      if($field['field_type'] === 'string') {
        return $value;
      }
      if($field['field_type'] === 'option') {
        return ['value' => $value];
      }
      if($field['field_type'] === 'option_multiple') {
        $list = [];
        foreach ($value as $option) {
          $list[] = ['value' => $option];
        }
        return $list;
      }
      if($field['field_type'] === 'number') {
        return floatval(
          preg_replace("/[^-0-9\.]/","", $value)
        );
      }

      throw new \Exception($this->t('field_type not found', ['@field_type' => $field['field_type']]));
    }
  }

  /**
   * @param $field
   * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
   *
   * @return array|null|string
   */
  protected function getValueByValueType($field, WebformSubmissionInterface $webform_submission) {
    $value = NULL;
    $data = $webform_submission->getData();
    if (!empty($field['value_type'])) {
      $value_type = $field['value_type'];
      if ($value_type == 'webform_element' && !empty($data[$field['value']])) {
        $value = $data[$field['value']];
      }
      if ($value_type == 'text' && !empty($field['value'])) {
        $value = $this->webformTokenManager->replace(
          $field['value'], $webform_submission, [], ['clear' => TRUE]
        );
      }
    }

    return $value;
  }

  /**
   * @param \JiraRestApi\Issue\Issue $issue
   * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
   *
   * @throws \JiraRestApi\JiraException
   * @throws \JsonMapper_Exception
   */
  protected function addAttachments(Issue $issue, WebformSubmissionInterface $webform_submission) {
    $webform = $webform_submission->getWebform();
    $attachment_webform_elements = $webform->getThirdPartySetting(
      'webform_jira',
      'attachment_webform_elements'
    );
    $attachments = [];
    foreach ($attachment_webform_elements as $attachment_webform_element) {
      $element = $webform->getElement($attachment_webform_element);
      /** @var \Drupal\webform\Plugin\WebformElementAttachmentInterface $element_plugin */
      $element_plugin = $this->webformElementManager->getElementInstance($element);
      $attachments = array_merge($attachments, $element_plugin->getAttachments($element, $webform_submission));
    }
    $this->jiraService->getIssueService($webform->getThirdPartySetting('webform_jira', 'endpoint'))->addAttachments($issue->id, array_column($attachments, 'filepath'));
  }
}
